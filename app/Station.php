<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $fillable = ["name","latitude","longitude","company_id"];

    public function company()
    {
        return $this->hasOne("App\Company","id","company_id");
    }
}