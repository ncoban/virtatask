<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ["name","parent_company_id"];

    public function stations()
    {
        return $this->hasMany("App\Station","company_id","id");
    }

    public function parent_company()
    {
        return $this->hasOne("App\Company","id","parent_company_id");
    }
}
