<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Station;

class StationController extends Controller
{
    public function index()
    {
        return [
            "error" => false,
            "data" => Station::with('company')->get()
        ];
    }
 
    public function show(Station $station)
    {
        return [
            "error"=>false,
            "data"=>$station
        ];
    }

    public function store(Request $request)
    {
        if($this->IsNullOrEmptyString(request('name'))){
            abort(400,'Please spesify a station name!');
        }
        $station = Station::create($request->all());
        return response()->json([
            "error"=>false,
            "data" => $station
        ], 201);
    }

    public function update(Request $request, Station $station)
    {
        if($this->IsNullOrEmptyString($station->name)){
            abort(400,'Please spesify a station name!');
        }
        $station->update($request->all());
        return response()->json([
            "error"=>false,
            "data" => $station
        ], 200);
    }

    public function delete(Station $station)
    {
        $station->delete();
        return response()->json(["error"=>false,"message"=>"The station removed successfully."], 200);
    }

    public function getNearest(Request $request)
    {
        $user_lat = $request->latitude;
        $user_lng = $request->longitude;
        if($this->IsNullOrEmptyString($user_lat) || $this->IsNullOrEmptyString($user_lng)){
            abort(400,"Please spesify latitude and longitude parameters!");
        }
        $companies = DB::SELECT("SELECT *
                                    FROM stations
                                        ORDER BY ((latitude-$user_lat)*(latitude-$user_lat)) + ((longitude - $user_lng)*(longitude - $user_lng)) ASC");
        return Response::json([
            "error" => false,
            "data" => $companies
        ]);
    }

    public function getStationsByCompany(Request $request){
        $company_id = $request->company_id;
        if($company_id == null){
            abort(400,"Please spesify a company id!");
        }
        $stations = DB::SELECT("SELECT * FROM stations T1 INNER JOIN
                                (SELECT id FROM companies WHERE id=@pv 
                                    UNION SELECT id FROM (SELECT id,parent_company_id FROM companies
                                        ORDER BY parent_company_id ASC, id ASC) companies_sorted,
                                        (SELECT @pv := '$company_id') initialisation
                                    WHERE find_in_set(parent_company_id, @pv)
                                    and LENGTH(@pv := concat(@pv, ',', id)) 
                                    UNION SELECT id FROM (SELECT id,parent_company_id FROM companies
                                        ORDER BY parent_company_id ASC, id DESC) companies_sorted,
                                        (SELECT @pv := '$company_id') initialisation
                                    WHERE find_in_set(parent_company_id, @pv)
                                    and LENGTH(@pv := concat(@pv, ',', id))
                                    UNION SELECT id FROM (SELECT id,parent_company_id FROM companies
                                        ORDER BY parent_company_id DESC, id ASC) companies_sorted,
                                        (SELECT @pv := '$company_id') initialisation
                                    WHERE find_in_set(parent_company_id, @pv)
                                    and LENGTH(@pv := concat(@pv, ',', id))
                                    UNION SELECT id FROM (SELECT id,parent_company_id FROM companies
                                        ORDER BY parent_company_id DESC, id DESC) companies_sorted,
                                        (SELECT @pv := '$company_id') initialisation
                                    WHERE find_in_set(parent_company_id, @pv)
                                    and LENGTH(@pv := concat(@pv, ',', id))) as T2 ON T1.company_id = T2.id");

        foreach ($stations as $station){
            $station->company=DB::table('companies')->where('id', $station->company_id)->first();
        }
        return Response::json([
            "error" => false,
            "data" => $stations
        ]);
    }

    // Function for basic field validation (present and neither empty nor only white space
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

}