<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Company;

class CompanyController extends Controller
{
    public function index()
    {
        return [
            "error" => false,
            "data" => Company::with(['stations','parent_company'])->get()
        ];
    }

    public function show(Company $company)
    {
        return [
            "error"=>false,
            "data"=>$company
        ];
    }

    public function store(Request $request)
    {
        if($this->IsNullOrEmptyString(request('name'))){
            abort(400,'Please spesify a company name!');
        }
        $company = Company::create($request->all());
        return response()->json([
            "error"=>false,
            "data" => $company
        ], 201);
    }

    public function update(Request $request, Company $company)
    {
        if($this->IsNullOrEmptyString(request('name'))){
            abort(400,'Please spesify a company name!');
        }
        $company->update($request->all());
        return response()->json([
            "error"=>false,
            "data" => $company
        ], 200);
    }

    public function delete(Company $company)
    {
        $company->delete();
        return response()->json(["error"=>false,"message"=>"The company removed successfully."], 200);
    }

    // Function for basic field validation (present and neither empty nor only white space
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
}