# Virta Task API

## Installation

1. Clone the repository `git clone git@bitbucket.org:ncoban/virtatask.git`
2. In the project, install dependencies using `composer install`

## Configuration

1. Copy `.env.example` to `.env` (`php -r "copy('.env.example', '.env');"`)
2. Generate the Laravel key using `php artisan key:generate`
3. Update `.env` file with your specific environment configuration (database credentials, api preferences, etc)
4. Generate the database schema using `php artisan migrate`

## Test setup
1. Run the tests ```./vendor/bin/phpunit```