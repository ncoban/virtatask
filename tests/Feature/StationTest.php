<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Station;

class StationTest extends TestCase
{
    public function testGetStationsompanies()
    {
        $response = $this->get('/api/station');
        $response->assertStatus(200);
    }

    public function testCreateStation()
    {
        $data = [
            "name" =>  "Test",
            "latitude" => 12,
            "longitude" => 13,
            "company_id" => 0
        ];
        $response = $this->json('POST', '/api/station', $data);
        $response
            ->assertStatus(201)
            ->assertJson([
                "error" => false,
                "data" => $data
            ]);
        $data = json_decode($response->content(), true);
        $id = $data["data"]["id"];
        Station::find($id)->delete();
    }

    public function testUpdateStation()
    {
        $station = factory(Station::class)->create();
        $id = $station->id;
        $data = [
            "name" =>  "Test Update",
            "latitude" => 12,
            "longitude" => 13,
            "company_id" => 0
        ];
        $response = $this->json('PUT', '/api/station/'.$id, $data);
        $response
            ->assertStatus(200)
            ->assertJson([
                "error" => false,
                "data" => $data
                ]);
        $station->delete();
    }

    public function testRemoveStation(){
        $station = factory(Station::class)->create();
        $id = $station->id;
        $response = $this->json('DELETE', '/api/station/'.$id);
        $response->assertStatus(200);
    }

}
