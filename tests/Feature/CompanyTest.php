<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Company;

class CompanyTest extends TestCase
{
    public function testGetCompanies()
    {
        $response = $this->get('/api/company');
        $response->assertStatus(200);
    }

    public function testCreateCompany()
    {
        $data = ['name' => 'Test','parent_company_id'=>0];
        $response = $this->json('POST', '/api/company', $data);
        $response
            ->assertStatus(201)
            ->assertJson([
                "error" => false,
                "data" => $data
            ]);
        $data = json_decode($response->content(), true);
        $id = $data["data"]["id"];
        Company::find($id)->delete();
    }

    public function testUpdateCompany()
    {
        $company = factory(Company::class)->create();
        $id = $company->id;
        $data = ["name" => "Test Update","parent_company_id"=>0];
        $response = $this->json('PUT', '/api/company/'.$id, $data);
        $response
            ->assertStatus(200)
            ->assertJson([
                "error" => false,
                "data" => $data
            ]);
        $company->delete();
    }

    public function testRemoveCompany(){
        $company = factory(Company::class)->create();
        $id = $company->id;
        $response = $this->json('DELETE', '/api/company/'.$id);
        $response->assertStatus(200);
    }

}
