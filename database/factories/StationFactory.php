<?php

use Faker\Generator as Faker;
use App\Station;

$factory->define(Station::class, function (Faker $faker) {
    return [
        'name'=>$faker->Company,
        'latitude'=>rand(0,90),
        'longitude'=>rand(0,180),
        'company_id'=>1
    ];
});
