<?php

use Illuminate\Database\Seeder;
use App\Station;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Station::class,1)->create();
    }
}
