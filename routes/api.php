<?php

use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\CheckApi as CheckApi;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('company', 'CompanyController@index');
Route::get('company/{company}', 'CompanyController@show');
Route::post('company', 'CompanyController@store');
Route::put('company/{company}', 'CompanyController@update');
Route::delete('company/{company}', 'CompanyController@delete');

Route::get('station', 'StationController@index');
Route::get('station/{station}', 'StationController@show');
Route::post('station', 'StationController@store');
Route::put('station/{station}', 'StationController@update');
Route::delete('station/{station}', 'StationController@delete');
Route::post('get_nearest', 'StationController@getNearest');
Route::post('stations_by_company', 'StationController@getStationsByCompany');